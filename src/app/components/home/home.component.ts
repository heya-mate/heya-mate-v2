import { Component, OnInit } from '@angular/core';

declare var $: any;
declare const ScrollReveal: any;

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

	constructor() { }

	ngOnInit() {
		/*“use strict” mode on*/
		"use strict";

		$(document).ready(function () {


			/////////////////////////////////////////////////////////////////
			// LOADER
			/////////////////////////////////////////////////////////////////




			// Page transition
			$('.main-menu a').on('click', function (e) {
				$('#page-preloader').fadeIn('slow');
			});


			// Transition delay
			$('.main-menu  a').click(function (e) {
				e.preventDefault();
				var goTo = this.getAttribute("href");
				setTimeout(function () {
					window.location = goTo;
				}, 500);
			});



			var $preloader = $('#page-preloader'),
				$spinner = $preloader.find('.spinner-loader');
			$spinner.fadeOut();
			$preloader.delay(1500).fadeOut('slow');



			/*=== Grid ====*/
			$('.pix_row').each(function (i) {
				if ($(this).find('.pix-item.y2').length != 0) {
					$(this).addClass('height-y2');
				}
			});

			/*=== Main slider ====*/
			$('#my-slider').sliderPro({
				width: 1170,
				height: 463,
				arrows: false,
				buttons: false,
				fade: true,
				waitForLayers: true,
				thumbnailPointer: false,
				autoplay: true,
				/*	waitForLayers: true,*/
				touchSwipe: false,
				autoScaleLayers: false
			});

			var mainSlider = $('#my-slider').data('sliderPro');

			/*=== Main slider prev button ====*/
			$('.main-slider-prev').on('click', function () {
				mainSlider.previousSlide();
				return false;
			});

			/*=== Main slider next button ====*/
			$('.main-slider-next').on('click', function () {
				mainSlider.nextSlide();
				return false;
			});

			/*=== Hover masks ====*/
			$(".hover-mask").each(function (i) {
				var $mask = $(this);
				var bckg = $mask.data('background');
				$(this).css("background-color", bckg);
			});

			$(".pix-item-wrap").each(function (i) {
				var $coloricon = $(this);
				var coloric = $coloricon.data('color');
				$(this).children('.pix-item-text').css("color", coloric);
				$(this).find('.border-bottom').css("border-color", coloric);
			});


			ScrollReveal({
				mobile: true,
				reset: true
			}
			).reveal('.scrollreveal');




			/*=== Owl gallery ====*/
			$(".enable-owl-carousel").each(function (i) {

				var $owl = $(this);

				var navigationData = $owl.data('navigation');
				var paginationData = $owl.data('pagination');
				var singleItemData = $owl.data('single-item');
				var autoPlayData = $owl.data('auto-play');
				var transitionStyleData = $owl.data('transition-style');
				var mainSliderData = $owl.data('main-text-animation');
				var afterInitDelay = $owl.data('after-init-delay');
				var stopOnHoverData = $owl.data('stop-on-hover');
				var min600 = $owl.data('min600');
				var min800 = $owl.data('min800');
				var min1200 = $owl.data('min1200');

				$owl.owlCarousel({
					navigation: navigationData,
					pagination: paginationData,
					singleItem: singleItemData,
					autoPlay: autoPlayData,
					transitionStyle: transitionStyleData,
					stopOnHover: stopOnHoverData,
					navigationText: ["", ""],
					itemsCustom: [
						[0, 1],
						[600, min600],
						[800, min800],
						[1200, min1200]
					],
				});
			});

			/*=== Form validation ====*/
			$.validate();

			/*=== Vertical menu ====*/
			$('#left-menu').metisMenu({
				toggle: false // disable the auto collapse. Default: true.
			});


		});
	}

}
